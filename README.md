Expense Tracker
===============
A simple expense tracker app developed with C#, ASP.NET and AngularJS.  
Key requirements and features are listed below:  
- The user must be able to create an account and log in.
- When logged in, user can see, edit and delete expenses he entered.
- Implement at least three roles with different permission levels: a regular user would only be able to CRUD on their owned records, a user manager would be able to CRUD users, and an admin would be able to CRUD all records and users.
- When an expense record is entered, each one has: date, time, description, amount, comment
- User can filter expenses.
- User can print expenses per week with total amount and average day spending.
- Minimal UI/UX design is needed.
- I need every client operation done using JavaScript, reloading the page is not an option.
- REST API. Make it possible to perform all user actions via the API, including authentication.
- You need to be able to pass credentials to both the webpage and the API.
- Bonus: unit and e2e tests!
- You will not be marked on graphic design, however, do try to keep it as tidy as possible.

Techs. used to build this project:  
- Visual Studio 2015.3
- .NET 4.6 / C# 5
- ASP.NET Web API 2
- ASP.NET Identity & OWIN
- SQL Server Exp.
- AngularJS 1.5
- Bootstrap, Font-Awesome, nprogress
- HTML/CSS/JavaScript