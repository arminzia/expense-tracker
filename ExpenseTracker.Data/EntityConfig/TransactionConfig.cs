﻿using ExpenseTracker.Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.EntityConfig
{
    // fluent API config for the transaction entity.
    public class TransactionConfig : EntityTypeConfiguration<Transaction>
    {
        public TransactionConfig()
        {
            this.Property(x => x.Description).HasMaxLength(150).HasColumnType("nvarchar");
            this.Property(x => x.Comment).HasMaxLength(500).HasColumnType("nvarchar");
        }
    }
}
