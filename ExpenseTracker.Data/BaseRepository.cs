﻿using ExpenseTracker.Common;
using ExpenseTracker.Data.Contracts;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Data
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        public IUnitOfWork UnitOfWork { get; set; }
        private IDbSet<TEntity> DbSet { get; set; }
        public DbSet<TEntity> DataSet { get; set; }

        public BaseRepository(IUnitOfWork uow)
        {
            this.UnitOfWork = uow;
            this.DbSet = this.UnitOfWork.Set<TEntity>();
            this.DataSet = this.DbSet as DbSet<TEntity>;
        }

        public OperationResult AddEntry(TEntity entity)
        {
            var result = new OperationResult() { Success = true };
            this.DbSet.Add(entity);

            var errors = this.GetValidationErrors();
            if (errors.Count() > 0)
            {
                result.Success = false;
                result.ValidationErrors = errors.ExtractValidationErrors();
            }

            return result;
        }

        public virtual OperationResult UpdateEntry(TEntity entity)
        {
            var result = new OperationResult();

            this.DbSet.Attach(entity);
            this.UnitOfWork.Entry(entity).State = EntityState.Modified;

            var errors = this.GetValidationErrors();
            if (errors.Count() > 0)
            {
                result.Success = false;
                result.ValidationErrors = errors.ExtractValidationErrors();

                return result;
            }

            result.Success = true;

            return result;
        }

        public OperationResult DeleteEntry(TEntity entity)
        {
            var result = new OperationResult();

            var entry = this.UnitOfWork.Entry(entity);
            if (entry != null)
            {
                this.DbSet.Remove(entity);
                result.Success = true;
            }

            return result;
        }

        public OperationResult DeleteEntry(int id)
        {
            var result = new OperationResult();

            var entry = this.DbSet.Find(id);
            if (entry != null)
            {
                this.DbSet.Remove(entry);
                result.Success = true;
            }

            return result;
        }

        public IEnumerable<DbEntityValidationResult> GetValidationErrors()
        {
            return this.UnitOfWork.GetValidationErrors();
        }
    }
}
