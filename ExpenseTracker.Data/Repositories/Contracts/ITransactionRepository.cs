﻿using ExpenseTracker.Data.Contracts;
using ExpenseTracker.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.Repositories.Contracts
{
    public interface ITransactionRepository : IBaseRepository<Transaction>
    {
    }
}
