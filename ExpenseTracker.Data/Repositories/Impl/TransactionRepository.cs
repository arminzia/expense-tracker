﻿using ExpenseTracker.Data.Contracts;
using ExpenseTracker.Data.Entities;
using ExpenseTracker.Data.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.Repositories.Impl
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IUnitOfWork uow) : base(uow) { }

    }
}
