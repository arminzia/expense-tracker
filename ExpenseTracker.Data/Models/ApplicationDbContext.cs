﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using ExpenseTracker.Data.Entities;
using ExpenseTracker.Data.EntityConfig;
using ExpenseTracker.Data.Contracts;
using System;

namespace ExpenseTracker.Data.Models
{
    public class ApplicationDbContext :
        IdentityDbContext<ApplicationUser, ApplicationRole, string, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>, IUnitOfWork
    {
        public ApplicationDbContext() : base("DefaultConnection") { }

        // expenses DbSet.
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // config base entity props.
            modelBuilder.Properties().Where(x => x.Name == "UpdatedAt")
                .Configure(x => x.IsOptional());
            modelBuilder.Properties().Where(x => x.Name == "UserId")
                .Configure(x => x.IsRequired().HasMaxLength(50));

            modelBuilder.Configurations.Add(new TransactionConfig());

            base.OnModelCreating(modelBuilder);
        }

        #region IUnitOfWork Members

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        #endregion
    }
}
