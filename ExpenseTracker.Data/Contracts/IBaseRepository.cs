﻿using ExpenseTracker.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Data.Contracts
{

    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IUnitOfWork UnitOfWork { get; set; }
        DbSet<TEntity> DataSet { get; set; }

        OperationResult AddEntry(TEntity entity);
        OperationResult UpdateEntry(TEntity entity);
        OperationResult DeleteEntry(TEntity entity);
        OperationResult DeleteEntry(int id);

        IEnumerable<DbEntityValidationResult> GetValidationErrors();
    }
}
