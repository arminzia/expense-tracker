﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Data
{
    public static class Helpers
    {
        public static List<string> ExtractValidationErrors(this IEnumerable<DbEntityValidationResult> errors)
        {
            List<string> messages = new List<string>();

            foreach (var error in errors)
            {
                foreach (var ve in error.ValidationErrors)
                {
                    messages.Add(ve.ErrorMessage);
                }
            }

            return messages;
        }

        public static IQueryable<T> PagedResult<T, TResult>(this IQueryable<T> query, int pageNum, int pageSize,
                        Expression<Func<T, TResult>> orderByProperty, bool isAscendingOrder, out int rowsCount)
        {
            if (pageSize <= 0) pageSize = 20;

            rowsCount = query.Count();

            if (rowsCount <= pageSize || pageNum <= 0) pageNum = 1;

            int excludedRows = (pageNum - 1) * pageSize;

            query = isAscendingOrder ? query.OrderBy(orderByProperty) : query.OrderByDescending(orderByProperty);

            return query.Skip(excludedRows).Take(pageSize);
        }
    }
}
