using ExpenseTracker.Data.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace ExpenseTracker.Data.Migrations
{
    public class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(ExpenseTracker.Data.Models.ApplicationDbContext context)
        {
            InitializeIdentityEF(context);
            base.Seed(context);
        }

        public static void InitializeIdentityEF(ApplicationDbContext context)
        {
            const string name = "admin@example.com";
            const string password = "Admin@123456";

            const string powerUserName = "poweruser@example.com";
            const string powerUserPass = "PowerUser@123456";

            const string _userRole = "User";
            const string _powerUserRole = "PowerUser";
            const string _adminRole = "Admin";

            // create system roles (Admin, PowerUser, User).
            ApplicationRole adminRole = new ApplicationRole { Name = _adminRole };
            ApplicationRole powerUserRole = new ApplicationRole { Name = _powerUserRole };
            if (!context.Roles.Any())
            {
                context.Roles.Add(new ApplicationRole { Name = _userRole });
                context.Roles.Add(powerUserRole);
                context.Roles.Add(adminRole);
            }
            else
            {
                adminRole = context.Roles.Where(r => r.Name == _adminRole).SingleOrDefault();
                powerUserRole = context.Roles.Where(r => r.Name == _powerUserRole).SingleOrDefault();
            }

            // create the admin user.
            var user = context.Users.Where(x => x.UserName == name).SingleOrDefault();
            if (user == null)
            {
                var hasher = new PasswordHasher();
                user = new ApplicationUser
                {
                    UserName = name,
                    Email = name,
                    Id = Guid.NewGuid().ToString(),
                    EmailConfirmed = true,
                    LockoutEnabled = true,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(password)
                };

                context.Users.Add(user);

                if (!user.Roles.Any())
                {
                    user.Roles.Add(new ApplicationUserRole { RoleId = adminRole.Id, UserId = user.Id });
                }
            }

            // create the power user.
            var powerUser = context.Users.Where(x => x.UserName == powerUserName).SingleOrDefault();
            if (powerUser == null)
            {
                var hasher = new PasswordHasher();
                powerUser = new ApplicationUser
                {
                    UserName = powerUserName,
                    Email = powerUserName,
                    Id = Guid.NewGuid().ToString(),
                    EmailConfirmed = true,
                    LockoutEnabled = true,  
                    SecurityStamp = Guid.NewGuid().ToString(),
                    PasswordHash = hasher.HashPassword(powerUserPass)
                };

                context.Users.Add(powerUser);

                if (!powerUser.Roles.Any())
                {
                    powerUser.Roles.Add(new ApplicationUserRole { RoleId = powerUserRole.Id, UserId = powerUser.Id });
                }
            }

            context.SaveChanges();
        }
    }
}
