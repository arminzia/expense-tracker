﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Services.Models
{
    // used to weekly report data.
    public class SampleResult
    {
        public int Day { get; set; }
        public decimal Sum { get; set; }
    }
}
