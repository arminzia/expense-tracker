﻿using ExpenseTracker.Common;
using ExpenseTracker.Data.Entities;
using ExpenseTracker.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Services.Contracts
{
    public interface ITransactionService
    {
        IEnumerable<Transaction> GetAll();
        IEnumerable<Transaction> GetAll(string userId);
        Transaction Get(int id);

        Task<OperationResult> Add(Transaction entity);
        Task<OperationResult> Update(Transaction entity);
        Task<OperationResult> Delete(int id);

        IEnumerable<Transaction> GetByDate(DateTime startDate, DateTime endDate);
        IEnumerable<SampleResult> GetWeeklyReport(string userId);

    }
}
