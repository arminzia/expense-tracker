﻿using ExpenseTracker.Data.Repositories.Contracts;
using ExpenseTracker.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpenseTracker.Common;
using ExpenseTracker.Data.Entities;
using System.Data.Entity;
using System.Diagnostics;
using ExpenseTracker.Services.Models;

namespace ExpenseTracker.Services.Impl
{
    public class TransactionService : ITransactionService
    {
        private ITransactionRepository _repo;

        public TransactionService(ITransactionRepository repo)
        {
            this._repo = repo;
        }

        #region CRUD

        public async Task<OperationResult> Add(Transaction entity)
        {
            var result = _repo.AddEntry(entity);
            if (result.ValidationErrors.Any())
                result.Message = result.ValidationErrors.ToValidationSummaryString();
            else
            {
                try
                {
                    int retVal = await _repo.UnitOfWork.SaveChangesAsync();
                    result.Success = true;
                    result.Message = "transaction saved.";
                    result.NewRecordId = entity.Id;
                }
                catch (Exception)
                {
                    result.Success = false;
                    result.Message = "couldn't save new transaction, sorry.";
                }
            }

            return result;
        }

        public async Task<OperationResult> Update(Transaction entity)
        {
            var result = _repo.UpdateEntry(entity);
            if (result.ValidationErrors.Any())
                result.Message = result.ValidationErrors.ToValidationSummaryString();
            else
            {
                try
                {
                    int retVal = await _repo.UnitOfWork.SaveChangesAsync();
                    result.Success = true;
                    result.Message = "transaction updated.";
                }
                catch (Exception)
                {
                    result.Success = false;
                    result.Message = "couldn't update transaction, sorry.";
                }
            }

            return result;
        }

        public async Task<OperationResult> Delete(int id)
        {
            var result = _repo.DeleteEntry(id);
            if (result.Success)
            {
                try
                {
                    int retVal = await _repo.UnitOfWork.SaveChangesAsync();

                    result.Success = true;
                    result.Message = "transaction removed.";
                }
                catch (Exception)
                {
                    result.Success = false;
                    result.Message = "couldn't remove transaction, sorry.";
                }
            }
            else
            {
                result.Message = "transaction not found!";
            }

            return result;
        }

        public Transaction Get(int id)
        {
            var transaction = _repo.DataSet.Find(id);
            return transaction;
        }

        public IEnumerable<Transaction> GetAll()
        {
            var transactions = _repo.DataSet.OrderByDescending(x => x.Date).ToList();
            return transactions;
        }

        public IEnumerable<Transaction> GetAll(string userId)
        {
            var transactions = _repo.DataSet
                .Where(x => x.UserId == userId)
                .OrderByDescending(x => x.Date).ToList();
            return transactions;
        }

        #endregion CRUD

        public IEnumerable<Transaction> GetByDate(DateTime startDate, DateTime endDate)
        {
            var transactions = _repo.DataSet
                .Where(x => x.Date >= startDate && x.Date <= endDate)
                .OrderByDescending(x => x.Date);

            return transactions.ToList();
        }

        public IEnumerable<SampleResult> GetWeeklyReport(string userId)
        {
            var startDate = DateTime.Now.AddDays(-7);
            var endDate = DateTime.Now;

            var result = from t in _repo.DataSet
                         where t.UserId == userId
                         where t.Date >= startDate && t.Date <= endDate
                         group t by t.Date.Day into g
                         select new SampleResult
                         {
                             Day = g.Key,
                             Sum = g.Sum(x => x.Amount)
                         };

            return result;
        }
    }
}
