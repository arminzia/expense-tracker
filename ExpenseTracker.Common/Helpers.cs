﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Common
{
    public static class Helpers
    {
        // extracts EF validation errors into a single string.
        public static string ToValidationSummaryString(this IEnumerable<string> errors)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var error in errors)
                sb.Append(string.Format("{0}<br/>", error));

            return sb.ToString();
        }
    }
}
