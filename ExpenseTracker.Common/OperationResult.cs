﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseTracker.Common
{
    // used for CRUD responses.
    public class OperationResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int NewRecordId { get; set; }
        public List<string> ValidationErrors { get; set; }

        public OperationResult()
        {
            this.ValidationErrors = new List<string>();
        }
    }
}
