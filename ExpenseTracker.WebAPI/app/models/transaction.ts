﻿class Transaction {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    userId: string;
    date: Date;
    amount: number;
    description: string;
    comment: string;
}