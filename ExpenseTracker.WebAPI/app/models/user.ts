﻿class User {
    id: string;
    userName: string;
    email: string;
    emailConfirmed: boolean;
    accessFailedCount: number;
    claims: Array<any>;
    lockoutEnabled: boolean;
    lockoutEndDateUtc?: Date;
    logins: Array<any>;
    passwordHash: string;
    phoneNumber?: string;
    phoneNumberConfirmed: boolean;
    roles: Array<any>;
    securityStamp: string;
    twoFactorEnabled: boolean;
}