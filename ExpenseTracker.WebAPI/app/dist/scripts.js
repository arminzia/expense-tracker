var ExpenseTracker;
(function (ExpenseTracker) {
    var Constants = (function () {
        function Constants() {
        }
        Object.defineProperty(Constants, "Default", {
            get: function () {
                return {
                    baseUrl: "http://localhost:64006/"
                };
            },
            enumerable: true,
            configurable: true
        });
        return Constants;
    }());
    ExpenseTracker.Constants = Constants;
    (function () {
        "use strict";
        var app = angular.module("expenseTracker", ['ui.router', 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'LocalStorageModule']);
        app.config(ConfigRoutes);
        app.config(ConfigStorage);
        ConfigRoutes.$inject = ["$stateProvider", "$urlRouterProvider"];
        function ConfigRoutes($stateProvider, $urlRouterProvider) {
            $stateProvider
                .state('home', {
                url: '/home',
                templateUrl: 'app/templates/home.html',
                controller: 'HomeController',
                controllerAs: 'ctrl'
            })
                .state('signup', {
                url: '/signup',
                templateUrl: 'app/templates/signup.html',
                controller: 'signupController',
                controllerAs: 'ctrl'
            })
                .state('signin', {
                url: '/signin',
                templateUrl: 'app/templates/signin.html',
                controller: 'SignInController',
                controllerAs: 'ctrl'
            })
                .state('users', {
                url: '/users',
                templateUrl: 'app/templates/users.html',
                controller: 'UsersController',
                controllerAs: 'ctrl'
            });
            $urlRouterProvider.otherwise('home');
        }
        ConfigStorage.$inject = ["$httpProvider", "localStorageServiceProvider"];
        function ConfigStorage($httpProvider, localStorageServiceProvider) {
            localStorageServiceProvider
                .setPrefix('expensetracker')
                .setStorageType('localStorage')
                .setNotify(true, true);
            // bearer-token interceptor
            var interceptor = function (localStorageService, $q, $location) {
                return {
                    request: function (config) {
                        var auth_data = localStorageService.get('auth_data');
                        if (auth_data !== null) {
                            config.headers['Authorization'] = "Bearer " + auth_data.access_token;
                        }
                        return config;
                    },
                    responseError: function (rejection) {
                        if (rejection.status === 401) {
                            return $q.reject(rejection);
                        }
                        if (rejection.status === 403) {
                            return $q.reject(rejection);
                        }
                        return $q.reject(rejection);
                    }
                };
            };
            // register interceptor
            var params = ['localStorageService', '$q', '$location'];
            interceptor.$inject = params;
            $httpProvider.interceptors.push(interceptor);
        }
        app.run(function ($rootScope, localStorageService, $state) {
            $rootScope.signOut = function () {
                localStorageService.remove('auth_data');
                $rootScope.authenticated = false;
                toastr.success("you signed out.", "Cya Soon!");
                $state.go('home');
            };
        });
    })();
})(ExpenseTracker || (ExpenseTracker = {}));
var Transaction = (function () {
    function Transaction() {
    }
    return Transaction;
}());
var User = (function () {
    function User() {
    }
    return User;
}());
var ExpenseTracker;
(function (ExpenseTracker) {
    var Services;
    (function (Services) {
        var TransactionService = (function () {
            function TransactionService($http, $q) {
                this.$http = $http;
                this.$q = $q;
                this.baseUrl = ExpenseTracker.Constants.Default.baseUrl;
            }
            TransactionService.prototype.getAll = function () {
                var url = this.baseUrl + "api/transactions";
                var deferred = this.$q.defer();
                this.$http.get(url).then(function (result) {
                    deferred.resolve(result.data);
                }, function (err) {
                    deferred.reject(err);
                });
                return deferred.promise;
            };
            TransactionService.prototype.getByDate = function (startDate, endDate) {
                var url = this.baseUrl + "api/transactions/getAllByDate";
                var deferred = this.$q.defer();
                console.info('start', startDate);
                console.info('end', endDate);
                this.$http({
                    method: 'POST',
                    url: url,
                    data: { startDate: startDate, endDate: endDate }
                }).then(function (result) {
                    deferred.resolve(result.data);
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            };
            TransactionService.prototype.addNew = function (model) {
                var url = this.baseUrl + "api/transactions";
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: url,
                    data: model
                }).then(function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                });
                return deferred.promise;
            };
            TransactionService.prototype.update = function (model) {
                var url = this.baseUrl + "api/transactions";
                var deferred = this.$q.defer();
                this.$http({
                    method: 'PUT',
                    url: url,
                    data: model
                }).then(function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                });
                return deferred.promise;
            };
            TransactionService.prototype.remove = function (id) {
                var url = this.baseUrl + "api/transactions/" + id;
                var deferred = this.$q.defer();
                this.$http({
                    method: 'DELETE',
                    url: url
                }).then(function (result) {
                    deferred.resolve(result);
                }, function (result) {
                    deferred.reject(result);
                });
                return deferred.promise;
            };
            return TransactionService;
        }());
        TransactionService.$inject = ["$http", "$q"];
        Services.TransactionService = TransactionService;
        angular.module('expenseTracker').service('TransactionService', TransactionService);
    })(Services = ExpenseTracker.Services || (ExpenseTracker.Services = {}));
})(ExpenseTracker || (ExpenseTracker = {}));
var ExpenseTracker;
(function (ExpenseTracker) {
    var Service;
    (function (Service) {
        var UsersService = (function () {
            function UsersService($http, $q) {
                this.$http = $http;
                this.$q = $q;
            }
            UsersService.prototype.getAll = function () {
                var url = ExpenseTracker.Constants.Default.baseUrl + "api/Users/GetAll";
                var deferred = this.$q.defer();
                NProgress.start();
                this.$http({
                    method: 'GET',
                    url: url
                }).then(function (response) {
                    deferred.resolve(response.data);
                }, function (response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            };
            UsersService.prototype.remove = function (id) {
                var url = ExpenseTracker.Constants.Default.baseUrl + "api/Users/Remove/";
                var deferred = this.$q.defer();
                this.$http({
                    method: 'POST',
                    url: url,
                    data: { userId: id },
                }).then(function (response) {
                    deferred.resolve(response);
                }, function (response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            };
            return UsersService;
        }());
        UsersService.$inject = ["$http", "$q"];
        Service.UsersService = UsersService;
        angular.module('expenseTracker').service('UsersService', UsersService);
    })(Service = ExpenseTracker.Service || (ExpenseTracker.Service = {}));
})(ExpenseTracker || (ExpenseTracker = {}));
var ExpenseTracker;
(function (ExpenseTracker) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var SignInController = (function () {
            function SignInController($scope, $state, $http, localStorageService) {
                this.$scope = $scope;
                this.$state = $state;
                this.$http = $http;
                this.localStorageService = localStorageService;
                this.baseUrl = ExpenseTracker.Constants.Default.baseUrl;
            }
            SignInController.prototype.signIn = function () {
                var _this = this;
                console.log('email', this.$scope.email);
                console.log('password', this.$scope.password);
                var payload = "grant_type=password&username=" + this.$scope.email + "&password=" + this.$scope.password;
                var url = this.baseUrl + "Token";
                this.$scope.xhrWorking = true;
                NProgress.start();
                this.$http({
                    method: 'POST',
                    url: url,
                    data: payload
                }).then(function (result) {
                    _this.localStorageService.set('auth_data', result.data);
                    toastr.success("yo wassup man!", "Welcome Back");
                    _this.$state.go('home');
                }, function (result) {
                    toastr.error(result.data.error_description, "Invalid Attempt");
                }).finally(function () {
                    _this.$scope.xhrWorking = false;
                    NProgress.done();
                });
            };
            return SignInController;
        }());
        SignInController.$inject = ["$scope", "$state", "$http", "localStorageService"];
        angular.module('expenseTracker').controller('SignInController', SignInController);
    })(Controllers = ExpenseTracker.Controllers || (ExpenseTracker.Controllers = {}));
})(ExpenseTracker || (ExpenseTracker = {}));
var ExpenseTracker;
(function (ExpenseTracker) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var HomeController = (function () {
            function HomeController($scope, $uibModal, $http, $rootScope, $timeout, localStorageService, transactionService, $document) {
                this.$scope = $scope;
                this.$uibModal = $uibModal;
                this.$http = $http;
                this.$rootScope = $rootScope;
                this.$timeout = $timeout;
                this.localStorageService = localStorageService;
                this.transactionService = transactionService;
                this.$document = $document;
                this.init();
            }
            HomeController.prototype.init = function () {
                var auth_data = this.localStorageService.get('auth_data');
                if (auth_data !== null) {
                    this.$scope.authenticated = true;
                    this.$rootScope.authenticated = true;
                }
                else {
                    this.$scope.authenticated = false;
                    this.$rootScope.authenticated = false;
                }
                if (this.$scope.authenticated) {
                    this.getTransactions();
                }
            };
            HomeController.prototype.getTransactions = function () {
                var _this = this;
                NProgress.start();
                this.transactionService.getAll()
                    .then(function (response) {
                    _this.$scope.transactions = response;
                }).finally(function () {
                    NProgress.done();
                });
            };
            HomeController.prototype.getWeekly = function () {
                var _this = this;
                var url = ExpenseTracker.Constants.Default.baseUrl + "api/Transactions/GetWeekly";
                NProgress.start();
                this.$http({
                    method: 'GET',
                    url: url
                }).then(function (response) {
                    console.log(response);
                    _this.$scope.weeklyData = response.data;
                    _this.$scope.weeklyTotal = 0;
                    angular.forEach(response.data, function (item) {
                        this.$scope.weeklyTotal += item.sum;
                    });
                }).finally(function () {
                    NProgress.done();
                });
            };
            HomeController.prototype.updateData = function (data) {
                this.$scope.transactions = data;
                var total = 0;
                angular.forEach(data, function (item) {
                    total += item.amount;
                });
                this.$scope.totalAmount = total;
            };
            HomeController.prototype.register = function (size, parentSelector) {
                var _this = this;
                var parentElem = parentSelector ?
                    angular.element(this.$document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
                var modalInstance = this.$uibModal.open({
                    animation: true,
                    ariaLabelledBy: 'modal-title',
                    ariaDescribedBy: 'modal-body',
                    templateUrl: 'app/templates/transaction-modal.html',
                    controller: 'ModalController',
                    controllerAs: '$ctrl',
                    size: size,
                    appendTo: parentElem,
                    resolve: {
                        transaction: function () {
                            return undefined;
                        }
                    }
                });
                modalInstance.result.then(function (model) {
                    NProgress.start();
                    _this.transactionService.addNew(model)
                        .then(function (response) {
                        var data = response.data;
                        toastr.success("added #" + data.Id, "Noice");
                        _this.$scope.transactions.push(data);
                    }, function (result) {
                        toastr.error(result.data.message);
                    }).finally(function () {
                        NProgress.done();
                    });
                }, function () {
                    console.info('Modal dismissed at: ' + new Date());
                });
            };
            HomeController.prototype.edit = function (model) {
                toastr.info("Not implemented yet!", "Edit Transaction");
            };
            HomeController.prototype.remove = function (model) {
                if (confirm('Are you sure you want to delete this transaction?')) {
                    NProgress.start();
                    this.transactionService.remove(model.id)
                        .then(function (response) {
                        toastr.success(response.data);
                    }).finally(function () {
                        NProgress.done();
                    });
                }
            };
            return HomeController;
        }());
        HomeController.$inject = ["$scope", "$uibModal", "$http", "$rootScope", "$timeout", "localStorageService", "TransactionService", "$log", "$document"];
        angular.module('expenseTracker').controller('HomeController', HomeController);
    })(Controllers = ExpenseTracker.Controllers || (ExpenseTracker.Controllers = {}));
})(ExpenseTracker || (ExpenseTracker = {}));
var ExpenseTracker;
(function (ExpenseTracker) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var UsersController = (function () {
            function UsersController($scope, $rootScope, $http, localStorageService, usersService) {
                this.$scope = $scope;
                this.$rootScope = $rootScope;
                this.$http = $http;
                this.localStorageService = localStorageService;
                this.usersService = usersService;
                this.init();
            }
            UsersController.prototype.init = function () {
                var auth_data = this.localStorageService.get('auth_data');
                if (auth_data !== null) {
                    this.$scope.authenticated = true;
                    this.$rootScope.authenticated = true;
                }
                else {
                    this.$scope.authenticated = false;
                    this.$rootScope.authenticated = false;
                }
                if (this.$scope.authenticated) {
                    this.getUsers();
                }
            };
            UsersController.prototype.getUsers = function () {
                var _this = this;
                NProgress.start();
                this.usersService.getAll()
                    .then(function (res) {
                    _this.$scope.users = res;
                }, function (res) {
                    if (res.status === 401) {
                        toastr.error(res.data.message);
                    }
                }).finally(function () {
                    NProgress.done();
                });
            };
            UsersController.prototype.edit = function (user) {
                toastr.info('Not implemented', 'User Management');
            };
            UsersController.prototype.remove = function (user) {
                var _this = this;
                if (confirm('Are you sure you want to remove this user?')) {
                    NProgress.start();
                    this.usersService.remove(user.id)
                        .then(function (res) {
                        toastr.success('Goodbye crewl world!', 'User Removed');
                        var index = _this.$scope.users.indexOf(user);
                        _this.$scope.users.splice(index, 1);
                    }).finally(function () {
                        NProgress.done();
                    });
                }
            };
            return UsersController;
        }());
        UsersController.$inject = ["$scope", "$rootScope", "$http", "localStorageService", "UsersService"];
        angular.module('expenseTracker').controller('UsersController', UsersController);
    })(Controllers = ExpenseTracker.Controllers || (ExpenseTracker.Controllers = {}));
})(ExpenseTracker || (ExpenseTracker = {}));
var ExpenseTracker;
(function (ExpenseTracker) {
    var Controllers;
    (function (Controllers) {
        "use strict";
        var ModalController = (function () {
            function ModalController($uibModalInstance, $scope, transaction) {
                this.$uibModalInstance = $uibModalInstance;
                this.$scope = $scope;
                this.transaction = transaction;
                this.init();
            }
            ModalController.prototype.init = function () {
                if (this.transaction !== undefined) {
                    this.$scope.model.id = this.transaction.id;
                    this.$scope.model.amount = this.transaction.amount;
                    this.$scope.model.date = this.transaction.date;
                    this.$scope.model.description = this.transaction.description;
                    this.$scope.model.comment = this.transaction.comment;
                }
            };
            ModalController.prototype.save = function () {
                if (this.$scope.amount < 1) {
                    toastr.error("Enter transaction amount!", "Can't Save Nothing");
                    return false;
                }
                if (this.$scope.amount < 1) {
                    toastr.error("Transaction amount is funny!", "Can't Save Funny Stuff");
                    return false;
                }
                var model = {
                    id: this.$scope.id,
                    amount: this.$scope.amount,
                    date: this.$scope.date,
                    description: this.$scope.description,
                    comment: this.$scope.comment
                };
                this.$uibModalInstance.close(model);
            };
            ModalController.prototype.cancel = function () {
                this.$uibModalInstance.dismiss('cancel');
            };
            return ModalController;
        }());
        ModalController.$inject = ["$uibModalInstance", "$scope", "transaction"];
        angular.module('expenseTracker').controller('ModalController', ModalController);
    })(Controllers = ExpenseTracker.Controllers || (ExpenseTracker.Controllers = {}));
})(ExpenseTracker || (ExpenseTracker = {}));
//# sourceMappingURL=scripts.js.map