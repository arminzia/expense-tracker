﻿module ExpenseTracker.Controllers {
    "use strict";

    interface IHomeControllerScope extends ng.IScope {
        transactions: Array<Transaction>;
        totalAmount: number;
        weeklyData: Array<any>;
        weeklyTotal: number;
        authenticated: boolean;
        transaction: Transaction;
    }

    interface IHomeController {
        init: () => void;
        getTransactions: () => void;
        updateData: (data: Array<Transaction>) => void;
        register: (size: any, parentSelector: any) => void;
        edit: (transaction: Transaction, size: any, parentSelector: any) => void;
        remove: (transaction: Transaction) => void;
        getWeekly: () => void;
    }

    class HomeController implements IHomeController {
        static $inject: string[] = ["$scope", "$uibModal", "$http", "$rootScope", "$timeout", "localStorageService", "TransactionService", "$log", "$document"];

        constructor(
            private $scope: IHomeControllerScope,
            private $uibModal: ng.ui.bootstrap.IModalService,
            private $http: ng.IHttpService,
            private $rootScope: any,
            private $timeout: ng.ITimeoutService,
            private localStorageService: ng.local.storage.ILocalStorageService,
            private transactionService: ExpenseTracker.Services.TransactionService,
            private $document: ng.IDocumentService) {

            this.init();
        }

        init() {
            var auth_data = this.localStorageService.get('auth_data');
            if (auth_data !== null) {
                this.$scope.authenticated = true;
                this.$rootScope.authenticated = true;
            } else {
                this.$scope.authenticated = false;
                this.$rootScope.authenticated = false;
            }

            if (this.$scope.authenticated) {
                this.getTransactions();
            }
        }

        getTransactions() {
            NProgress.start();
            this.transactionService.getAll()
                .then(response => {
                    this.$scope.transactions = response as Array<Transaction>;
                }).finally(() => {
                    NProgress.done();
                });
        }

        getWeekly() {
            let url = ExpenseTracker.Constants.Default.baseUrl + "api/Transactions/GetWeekly";
            NProgress.start();
            this.$http({
                method: 'GET',
                url: url
            }).then(response => {
                console.log(response);

                this.$scope.weeklyData = response.data as Array<any>;

                this.$scope.weeklyTotal = 0;
                angular.forEach(response.data, function (item) {
                    this.$scope.weeklyTotal += item.sum;
                });
            }).finally(() => {
                NProgress.done();
            });
        }

        updateData(data: any) {
            this.$scope.transactions = data;

            let total = 0;
            angular.forEach(data, function (item) {
                total += item.amount;
            });

            this.$scope.totalAmount = total;
        }

        register(size: any, parentSelector: any) {
            let parentElem = parentSelector ?
                angular.element(this.$document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;

            let modalInstance = this.$uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/templates/transaction-modal.html',
                controller: 'ModalController',
                controllerAs: '$ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    transaction: function () {
                        return undefined;
                    }
                }
            });

            modalInstance.result.then(model => {
                NProgress.start();
                this.transactionService.addNew(model)
                    .then((response: any) => {
                        let data = response.data;
                        toastr.success("added #" + data.Id, "Noice");
                        this.$scope.transactions.push(data);
                    }, (result) => {
                        toastr.error(result.data.message);
                    }).finally(() => {
                        NProgress.done();
                    });
            }, function () {
                console.info('Modal dismissed at: ' + new Date());
            });
        }

        edit(model: Transaction) {
            toastr.info("Not implemented yet!", "Edit Transaction");
        }

        remove(model: Transaction) {
            if (confirm('Are you sure you want to delete this transaction?')) {
                NProgress.start();
                this.transactionService.remove(model.id)
                    .then((response: any) => {
                        toastr.success(response.data);
                    }).finally(() => {
                        NProgress.done();
                    });
            }
        }

    }

    angular.module('expenseTracker').controller('HomeController', HomeController);

}