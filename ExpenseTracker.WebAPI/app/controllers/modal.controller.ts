﻿module ExpenseTracker.Controllers {
    "use strict";

    interface IModalControllerScope extends ng.IScope {
        id: number;
        amount: number;
        date: Date;
        description: string;
        comment: string;
    }

    interface IModalController {
        init: () => void;
        save: () => void;
        cancel: () => void;
    }

    class ModalController implements IModalController {
        static $inject: string[] = ["$uibModalInstance", "$scope", "transaction"];
        constructor(
            private $uibModalInstance: angular.ui.bootstrap.IModalInstanceService,
            private $scope: IModalControllerScope,
            private transaction: Transaction) {
            this.init();
        }

        init() {
            if (this.transaction !== undefined) {
                this.$scope.model.id = this.transaction.id;
                this.$scope.model.amount = this.transaction.amount;
                this.$scope.model.date = this.transaction.date;
                this.$scope.model.description = this.transaction.description;
                this.$scope.model.comment = this.transaction.comment;
            }
        }

        save() {
            if (this.$scope.amount < 1) {
                toastr.error("Enter transaction amount!", "Can't Save Nothing");
                return false;
            }

            if (this.$scope.amount < 1) {
                toastr.error("Transaction amount is funny!", "Can't Save Funny Stuff");
                return false;
            }

            let model = {
                id: this.$scope.id,
                amount: this.$scope.amount,
                date: this.$scope.date,
                description: this.$scope.description,
                comment: this.$scope.comment
            };

            this.$uibModalInstance.close(model);
        }

        cancel() {
            this.$uibModalInstance.dismiss('cancel');
        }
    }

    angular.module('expenseTracker').controller('ModalController', ModalController);
}