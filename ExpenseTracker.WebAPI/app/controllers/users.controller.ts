﻿module ExpenseTracker.Controllers {
    "use strict";

    interface IUsersControllerScope extends angular.IScope {
        users: Array<User>;
        authenticated: boolean;
    }

    interface IUsersController {
        init: () => void;
        getUsers: () => void;
        edit: (user: User) => void;
        remove: (user: User) => void;
    }

    class UsersController implements IUsersController {
        static $inject: string[] = ["$scope", "$rootScope", "$http", "localStorageService", "UsersService"];

        constructor(
            private $scope: IUsersControllerScope,
            private $rootScope: any,
            private $http: angular.IHttpService,
            private localStorageService: angular.local.storage.ILocalStorageService,
            private usersService: ExpenseTracker.Service.UsersService) {
            this.init();
        }
        
        init() {
            let auth_data = this.localStorageService.get('auth_data');
            if (auth_data !== null) {
                this.$scope.authenticated = true;
                this.$rootScope.authenticated = true;
            } else {
                this.$scope.authenticated = false;
                this.$rootScope.authenticated = false;
            }

            if (this.$scope.authenticated) {
                this.getUsers();
            }
        }

        getUsers() {
            NProgress.start();
            this.usersService.getAll()
                .then(res => {
                    this.$scope.users = res as Array<User>;
                }, res => {
                    if (res.status === 401) {
                        toastr.error(res.data.message);
                    }
                }).finally(() => {
                    NProgress.done();
                });
        }

        edit(user: User) {
            toastr.info('Not implemented', 'User Management');
        }

        remove(user: User) {
            if (confirm('Are you sure you want to remove this user?')) {
                NProgress.start();
                this.usersService.remove(user.id)
                    .then(res => {
                        toastr.success('Goodbye crewl world!', 'User Removed');
                        let index = this.$scope.users.indexOf(user);
                        this.$scope.users.splice(index, 1);
                    }).finally(() => {
                        NProgress.done();
                    });
            }
        }
    }

    angular.module('expenseTracker').controller('UsersController', UsersController);
}