﻿module ExpenseTracker.Controllers {
    "use strict";

    interface ISignInControllerScope extends ng.IScope {
        email: string;
        password: string;
        xhrWorking: boolean;
    }

    interface ISignInController {
        signIn: () => void;
    }

    class SignInController implements ISignInController {
        static $inject: string[] = ["$scope", "$state", "$http", "localStorageService"];
        private baseUrl: string = ExpenseTracker.Constants.Default.baseUrl;

        constructor(
            private $scope: ISignInControllerScope,
            private $state: any,
            private $http: ng.IHttpService,
            private localStorageService: ng.local.storage.ILocalStorageService) {
        }

        signIn() {
            console.log('email', this.$scope.email);
            console.log('password', this.$scope.password);

            let payload = "grant_type=password&username=" + this.$scope.email + "&password=" + this.$scope.password;
            let url = this.baseUrl + "Token";

            this.$scope.xhrWorking = true;
            NProgress.start();
            this.$http({
                method: 'POST',
                url: url,
                data: payload
            }).then((result) => {
                this.localStorageService.set('auth_data', result.data);
                toastr.success("yo wassup man!", "Welcome Back");

                this.$state.go('home');
            }, (result) => {
                toastr.error(result.data.error_description, "Invalid Attempt");
            }).finally(() => {
                this.$scope.xhrWorking = false;
                NProgress.done();
            });
        }
    }

    angular.module('expenseTracker').controller('SignInController', SignInController);
}