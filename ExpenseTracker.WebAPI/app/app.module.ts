﻿module ExpenseTracker {
    export class Constants {
        static get Default(): any {
            return {
                baseUrl: "http://localhost:64006/"
            }
        }
    }

    ((): void => {
        "use strict";

        let app = angular.module("expenseTracker", ['ui.router', 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'LocalStorageModule']);

        app.config(ConfigRoutes);
        app.config(ConfigStorage);

        ConfigRoutes.$inject = ["$stateProvider", "$urlRouterProvider"];
        function ConfigRoutes($stateProvider: angular.ui.IStateProvider, $urlRouterProvider: angular.ui.IUrlRouterProvider) {
            $stateProvider
                .state('home', {
                    url: '/home',
                    templateUrl: 'app/templates/home.html',
                    controller: 'HomeController',
                    controllerAs: 'ctrl'
                })

                .state('signup', {
                    url: '/signup',
                    templateUrl: 'app/templates/signup.html',
                    controller: 'signupController',
                    controllerAs: 'ctrl'
                })

                .state('signin', {
                    url: '/signin',
                    templateUrl: 'app/templates/signin.html',
                    controller: 'SignInController',
                    controllerAs: 'ctrl'
                })

                .state('users', {
                    url: '/users',
                    templateUrl: 'app/templates/users.html',
                    controller: 'UsersController',
                    controllerAs: 'ctrl'
                });

            $urlRouterProvider.otherwise('home');
        }

        ConfigStorage.$inject = ["$httpProvider", "localStorageServiceProvider"];
        function ConfigStorage($httpProvider: angular.IHttpProvider, localStorageServiceProvider: angular.local.storage.ILocalStorageServiceProvider) {
            localStorageServiceProvider
                .setPrefix('expensetracker')
                .setStorageType('localStorage')
                .setNotify(true, true)

            // bearer-token interceptor
            var interceptor = function (localStorageService, $q, $location) {
                return {
                    request: function (config) {
                        var auth_data = localStorageService.get('auth_data');
                        if (auth_data !== null) {
                            config.headers['Authorization'] = "Bearer " + auth_data.access_token;
                        }
                        return config;
                    },
                    responseError: function (rejection) {
                        if (rejection.status === 401) {
                            return $q.reject(rejection);
                        }
                        if (rejection.status === 403) {
                            return $q.reject(rejection);
                        }

                        return $q.reject(rejection);
                    }
                };
            };

            // register interceptor
            var params = ['localStorageService', '$q', '$location'];
            interceptor.$inject = params;
            $httpProvider.interceptors.push(interceptor);
        }

        app.run(($rootScope, localStorageService, $state) => {
            $rootScope.signOut = () => {
                localStorageService.remove('auth_data');
                $rootScope.authenticated = false;
                
                toastr.success("you signed out.", "Cya Soon!");
                $state.go('home');
            }
        });
    })();
}