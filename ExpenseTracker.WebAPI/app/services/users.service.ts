﻿module ExpenseTracker.Service {
    interface IUsersService {
        getAll: () => angular.IHttpPromise<any>;
        remove: (id: string) => angular.IHttpPromise<any>;
    }

    export class UsersService implements IUsersService {
        static $inject: string[] = ["$http", "$q"];
        constructor(private $http: angular.IHttpService, private $q: angular.IQService) { }

        getAll() {
            let url = ExpenseTracker.Constants.Default.baseUrl + "api/Users/GetAll";
            let deferred = this.$q.defer();

            NProgress.start();
            this.$http({
                method: 'GET',
                url: url
            }).then(response => {
                deferred.resolve(response.data);
            }, response => {
                deferred.reject(response);
            });

            return deferred.promise;
        }

        remove(id: string) {
            let url = ExpenseTracker.Constants.Default.baseUrl + "api/Users/Remove/";
            let deferred = this.$q.defer();
            
            this.$http({
                method: 'POST',
                url: url,
                data: { userId: id },
            }).then(response => {
                deferred.resolve(response);
            }, response => {
                deferred.reject(response);
            });

            return deferred.promise;
        }
    }

    angular.module('expenseTracker').service('UsersService', UsersService);
}