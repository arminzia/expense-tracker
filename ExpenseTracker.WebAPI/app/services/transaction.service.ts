﻿module ExpenseTracker.Services {
    interface ITransactionService {
        getAll: () => angular.IHttpPromise<any>;
        getByDate: (startDate: Date, endDate: Date) => angular.IPromise<any>;
        addNew: (model: Transaction) => void;
        update: (model: Transaction) => void;
        remove: (id: number) => void;
    }

    export class TransactionService implements ITransactionService {
        static $inject = ["$http", "$q"];
        private baseUrl: string = ExpenseTracker.Constants.Default.baseUrl;

        constructor(private $http: angular.IHttpService, private $q: angular.IQService) {

        }

        getAll() {
            let url = this.baseUrl + "api/transactions";
            let deferred = this.$q.defer();
            this.$http.get(url).then((result) => {
                deferred.resolve(result.data);
            }, (err) => {
                deferred.reject(err);
            });

            return deferred.promise;
        }

        getByDate(startDate: Date, endDate: Date) {
            let url = this.baseUrl + "api/transactions/getAllByDate";
            let deferred = this.$q.defer();

            console.info('start', startDate);
            console.info('end', endDate);

            this.$http({
                method: 'POST',
                url: url,
                data: { startDate: startDate, endDate: endDate }
            }).then(function (result) {
                deferred.resolve(result.data);
            }, function (error) {
                deferred.reject(error);
            });

            return deferred.promise;
        }

        addNew(model: Transaction) {
            let url = this.baseUrl + "api/transactions";
            let deferred = this.$q.defer();

            this.$http({
                method: 'POST',
                url: url,
                data: model
            }).then(function (result) {
                deferred.resolve(result);
            }, function (result) {
                deferred.reject(result);
            });

            return deferred.promise;
        }

        update(model: Transaction) {
            let url = this.baseUrl + "api/transactions";
            let deferred = this.$q.defer();
            this.$http({
                method: 'PUT',
                url: url,
                data: model
            }).then(function (result) {
                deferred.resolve(result);
            }, function (result) {
                deferred.reject(result);
            });

            return deferred.promise;
        }

        remove(id: number) {
            let url = this.baseUrl + "api/transactions/" + id;
            let deferred = this.$q.defer();
            this.$http({
                method: 'DELETE',
                url: url
            }).then(function (result) {
                deferred.resolve(result);
            }, function (result) {
                deferred.reject(result);
            });

            return deferred.promise;
        }
    }

    angular.module('expenseTracker').service('TransactionService', TransactionService);
}