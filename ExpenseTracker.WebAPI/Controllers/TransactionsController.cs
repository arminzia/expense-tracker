﻿using ExpenseTracker.Data.Entities;
using ExpenseTracker.Data.Models;
using ExpenseTracker.Services.Contracts;
using ExpenseTracker.WebAPI.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ExpenseTracker.WebAPI.Controllers
{
    [Authorize(Roles = "Admin, User")]
    [RoutePrefix("api/Transactions")]
    public class TransactionsController : ApiController
    {
        private ITransactionService _service;

        public TransactionsController(ITransactionService service)
        {
            this._service = service;
        }

        // GET api/transactions
        public async Task<IHttpActionResult> Get()
        {
            // get user manager and current user
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var principal = RequestContext.Principal;
            var user = await userManager.FindByNameAsync(principal.Identity.Name);

            var result = new List<Transaction>();
            if (await userManager.IsInRoleAsync(user.Id, "Admin"))
            {
                result = _service.GetAll().ToList();
            }
            else
            {
                result = _service.GetAll(user.Id).ToList();
            }

            return Ok(result);
        }

        public IHttpActionResult Get(int id)
        {
            var transaction = _service.Get(id);
            if (transaction == null)
            {
                return Ok(new Transaction());
            }

            return Ok(transaction);
        }

        [Route("GetWeekly")]
        public async Task<IHttpActionResult> GetWeekly()
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var principal = RequestContext.Principal;
            var user = await userManager.FindByNameAsync(principal.Identity.Name);
            
            var result = _service.GetWeeklyReport(user.Id);
            return Ok(result);
        }

        [HttpPost]
        [Route("GetAllByDate")]
        public IHttpActionResult GetAllByDate(GetByDateDTO model)
        {
            var transactions = _service.GetByDate(model.StartDate, model.EndDate);
            if (transactions == null)
            {
                return Ok(new List<Transaction>());
            }

            return Ok(transactions);
        }

        [HttpPost]
        public async Task<IHttpActionResult> Post(Transaction model)
        {
            if (model == null)
            {
                return BadRequest("model data is null!");
            }

            // get user manager and current user
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var principal = RequestContext.Principal;
            var user = await userManager.FindByNameAsync(principal.Identity.Name);

            // add new bank account
            model.UserId = user.Id;
            model.CreatedAt = DateTime.Now;

            var result = await _service.Add(model);
            if (result.Success)
            {
                // transaction created
                //return CreatedAtRoute("Get", result.NewRecordId, result.Message);
                return Ok(model);
            }
            else
            {
                return BadRequest(result.Message);
            }
        }

        [HttpPut]
        public async Task<IHttpActionResult> Put(Transaction model)
        {
            if (model == null)
            {
                return BadRequest("model data is null!");
            }

            // get user manager and current user
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var principal = RequestContext.Principal;
            var user = await userManager.FindByNameAsync(principal.Identity.Name);

            // update category
            model.UpdatedAt = DateTime.Now;
            var result = await _service.Update(model);
            if (result.Success)
            {
                // bank account updated
                return Ok(result.Message);
            }
            else
            {
                // if any validation errors
                if (result.ValidationErrors.Any())
                {
                    return BadRequest(result.Message);
                }
                // if there was a server error
                return Content(HttpStatusCode.InternalServerError, result.Message);
            }
        }

        public async Task<IHttpActionResult> Delete(int id)
        {
            var result = await _service.Delete(id);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return Content(HttpStatusCode.InternalServerError, result.Message);
        }
    }
}
