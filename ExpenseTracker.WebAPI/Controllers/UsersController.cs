﻿using ExpenseTracker.Data.Models;
using ExpenseTracker.WebAPI.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ExpenseTracker.WebAPI.Controllers
{
    [Authorize(Roles = "Admin, PowerUser")]
    [RoutePrefix("api/Users")]
    public class UsersController : ApiController
    {
        public IHttpActionResult GetAll()
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var users = userManager.Users.ToList();

            return Ok(users);
        }

        [HttpPost]
        [Route("Remove")]
        public async Task<IHttpActionResult> Remove(DeleteUserDTO model)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = await userManager.FindByIdAsync(model.UserId);
            if (user != null)
            {
                var result = await userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return Ok("User deleted successfully.");
                }

                return BadRequest();
            }

            return NotFound();
        }
    }
}
