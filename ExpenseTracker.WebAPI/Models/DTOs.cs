﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExpenseTracker.WebAPI.Models
{
    public class GetByDateDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class DeleteUserDTO
    {
        public string UserId { get; set; }
    }

}